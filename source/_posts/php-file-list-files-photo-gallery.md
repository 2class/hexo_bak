---
title: 漂亮的PHP单文件目录列表-files.photo.gallery
date: 2022-08-24 15:03:07
tags:
	- files.photo.gallery
	- php
	- php目录列表
categories: 软件资源
---
files.photo.gallery是一个漂亮的PHP单文件目录列表工具，可以帮助用户快速生成美观的文件列表页面。该工具支持多种文件格式的展示，包括图片、视频、音频、文档等。用户可以通过简单的配置，自定义页面的风格和样式，以满足自己的需求。此外，该工具还支持多种语言，方便不同地区的用户使用。
![](https://img.baxx.eu.org/202301272253744.png)
<!--more-->
# 演示
https://files.photo.gallery/demo

# 下载链接
https://cdn.jsdelivr.net/npm/files.photo.gallery/index.php

# 开心
上传、删除、改名提示License required
index.php配置
license_key 留空不填
删去
```
if(!config::$config['license_key']) json_error('License required!');
```

file.js
```
https://onedrive.baxx.eu.org/api/raw/?path=/files.photo.gallery/files.js
```
