---
title: Windows PE推荐：磁盘管理、文件管理、注册表编辑器等实用工具
date: 2022-08-23 09:19:01
tags:
	- WindowsPE
	- PE
categories: 软件资源
---
Windows PE是一款非常实用的Windows预安装环境，它可以在没有安装Windows的情况下，运行一些Windows应用程序和工具。Windows PE可以作为Windows系统的故障诊断和恢复工具，以及安装和部署Windows系统的工具。在Windows PE中，用户可以使用多种Windows工具，如磁盘管理、文件管理、注册表编辑器、网络工具等，非常方便。同时，Windows PE还支持自定义和扩展，用户可以根据自己的需求添加或删除工具。
<!--more-->
WePE:
http://www.wepe.cn/download.html

FirPE:
https://firpe.cn/page-247

Edgeless:
https://down.edgeless.top/
