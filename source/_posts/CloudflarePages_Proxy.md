---
title: CloudflarePages反向代理
date: 2022-08-22 12:14:42
tags:
	- CloudflarePages
	- Cloudflare
	- 反向代理
categories: 免费资源
---
Cloudflare Pages是一项托管服务，允许开发人员将其静态网站和Web应用程序部署到全球性的CDN上。通过反向代理，Cloudflare Pages可以帮助开发人员加速其网站和应用程序的加载速度，并提供可靠的安全性和可扩展性。此外，Cloudflare Pages还具有自动构建和部署功能，使开发人员可以更轻松地管理其网站和应用程序的生命周期。
<!--more-->
![](https://img.baxx.eu.org/202301272256903.png)
# 准备
需要的材料:
Cloudflare账户
Github/Gitlab账户
# 教程
1.新建一个Github/Gitlab仓库
2.创建_worker.js文件
```
export default {
  async fetch(request, env) {
    let url = new URL(request.url);
    if (url.pathname.startsWith('/')) {
      url.hostname = '此处修改为反代的网站地址'
      let new_request = new Request(url, request);
      return fetch(new_request);
    }
    return env.ASSETS.fetch(request);
  },
};
```
3.转到CloudflarePages进行部署
