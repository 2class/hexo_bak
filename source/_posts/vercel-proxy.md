---
title: 使用Vercel反代网站
date: 2022-08-24 13:22:35
tags:
	- Vercel
	- 反向代理
	- CDN
	- 反代
categories: 技术教程
---
使用Vercel反代网站可以将网站部署在全球多个节点，加速网站访问速度，同时减轻原始网站的服务器负担。通过Vercel的CDN加速和缓存机制，可大幅提升网站的响应速度和稳定性，同时减少了带宽和服务器资源的消耗。此外，Vercel还提供了HTTPS支持，保证网站访问的安全性。通过这种方式，用户可以更快、更安全地访问网站，同时降低了网站运维成本。
<!--more-->
# 准备
一个[Vercel](https://vercel.com)账户
安装好[NodeJS](https://nodejs.org/zh-cn/)环境

# 安装Vercel CLI
```
npm i -g vercel
```

# 登录Vercel
```
vercel login
```

# 创建 xxx.json 文件(文件名随意)
```
{
  "version": 2,
  "routes": [
      {"src": "/(.*)","dest": "http://example.com/$1"}
  ]
}
```
将上面的```http://example.com/```改为你想要反代的URL

# 部署
```
vercel -A xxx.json --prod
```
