---
title: 免费获取IBMLinuxOneVPS服务器
date: 2022-08-21 19:24:38
tags: 
	- linuxone
	- ibm
	- vps
categories: 免费资源
---
IBM LinuxOne是一种高性能、安全、可靠的服务器，基于Linux操作系统，适用于企业级应用和数据处理。它采用了IBM独有的硬件安全技术，包括加密协处理器、硬件安全模块和物理隔离等，确保数据安全和保密性。同时，它还具有高可用性和弹性，支持多种虚拟化和容器化技术，可以满足不同的业务需求。通过免费获取IBM LinuxOne服务器，用户可以体验到其卓越的性能和安全性。
<!--more-->
# 准备
一个邮箱，推荐 GMail，国内可以 Outlook，不推荐 iCloud、QQ
英文基础，看不懂英文请使用浏览器翻译插件
Linux 基础
# 教程
先进[官网](https://linuxone.cloud.marist.edu/#/register?flag=VM)注册，需要填写一些基本信息，地区写中国就可以了，如果是美国要验证地址和电话，申请理由可以写“Building Android”“Machine Learning”“develop website”之类的，如果描述不太清楚可能审核不通过
![](https://files.baxx.eu.org/img/202208211954865.png)

注册账户有概率出现人工审核，一般是由于你开了代理或者邮箱不好，注册好了邮箱会收到一封邮件，点击邮件里链接验证，验证好了会受到另一封邮件，里面有账户的过期时间
然后去创建实例，系统一般选择 RHEL（CentOS 的妈）或 Ubuntu，配置直接选择 2 核 4 GB 就可以
创建好了去生成一下 SSH 密钥，这台机子默认没启用密码登录
```
ssh-keygen -t rsa
```
![](https://files.baxx.eu.org/img/202208211958043.png)

把 SSH 密钥拷到 LinuxOne 机器上，然后就可以用这个密钥连接了，账号名是 linux1，登陆成功会有下面的消息
```
>___ ____  __  __      _     _                   ___  _   _ _____
|_ _| __ )|  \/  |    | |   (_)_ __  _   ___  __/ _ \| \ | | ____|
 | ||  _ \| |\/| |    | |   | | '_ \| | | \ \/ / | | |  \| |  _|
 | || |_) | |  | |    | |___| | | | | |_| |>  <| |_| | |\  | |___
|___|____/|_|  |_|    |_____|_|_| |_|\__,_/_/\_\\___/|_| \_|_____|

=================================================================================
Welcome to the IBM LinuxONE Community Cloud!

This server is for authorized users only. All activity is logged and monitored.
Individuals using this server must abide to the Terms and Conditions listed here:
https://www.ibm.com/community/z/ibm-linuxone-community-cloud-terms-and-conditions/
Your access will be revoked for any non-compliance.
==================================================================================
```
## SSH 开启密码登录
```
vim /etc/ssh/sshd_config

```
![](https://files.baxx.eu.org/img/202208212003774.png)

![](https://files.baxx.eu.org/img/202208211959519.png)
```
[ESC]:wq[Enter]
systemctl restart sshd
```
## 机器测试
测试一些基本、常用的程序和功能

### uname -a
``` uname -a
Linux MBR390 3.10.0-1160.59.1.el7.s390x #1 SMP Wed Feb 16 07:14:49 EST 2022 s390x s390x s390x GNU/Linux
```
### Docker
能装，但是要注意容器是否提供 S390x 架构
```
WARNING: The requested image's platform (linux/amd64) does not match the detected host platform (linux/s390x) and no specific platform was requested
```
### LNMP/LAMP
能装，用包管理器，yum 或 apt

### cpuinfo
5.2 GHz，960MB 四级缓存
```
$ cat /proc/cpuinfo
vendor_id       : IBM/S390
# processors    : 2
bogomips per cpu: 3241.00
max thread id   : 0
features        : esan3 zarch stfle msa ldisp eimm dfp edat etf3eh highgprs te vx vxd vxe gs vxe2 vxp sort dflt sie
facilities      : 0 1 2 3 4 6 7 8 9 10 12 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 30 31 32 33 34 35 36 37 38 40 41 42 43 44 45 46 47 48 49 50 51 52 53 54 55 57 58 59 60 61 73 74 75 76 77 80 81 82 128 129 130 131 133 134 135 146 147 148 150 151 152 155 156 168
cache0          : level=1 type=Data scope=Private size=128K line_size=256 associativity=8
cache1          : level=1 type=Instruction scope=Private size=128K line_size=256 associativity=8
cache2          : level=2 type=Data scope=Private size=4096K line_size=256 associativity=8
cache3          : level=2 type=Instruction scope=Private size=4096K line_size=256 associativity=8
cache4          : level=3 type=Unified scope=Shared size=262144K line_size=256 associativity=32
cache5          : level=4 type=Unified scope=Shared size=983040K line_size=256 associativity=60
processor 0: version = FF,  identification = 0418E8,  machine = 8561
processor 1: version = FF,  identification = 0418E8,  machine = 8561

cpu number      : 0
cpu MHz dynamic : 5200
cpu MHz static  : 5200

cpu number      : 1
cpu MHz dynamic : 5200
cpu MHz static  : 5200
```

### neofetch
```
RHEL       .MMM..:MMMMMMM                  root@MBR390
          MMMMMMMMMMMMMMMMMM               -----------
          MMMMMMMMMMMMMMMMMMMM.            OS: Red Hat Enterprise Linux Server 7.9 (Maipo) s390x
         MMMMMMMMMMMMMMMMMMMMMM            Kernel: 3.10.0-1160.59.1.el7.s390x
        ,MMMMMMMMMMMMMMMMMMMMMM:           Uptime: 7 hours, 30 mins
        MMMMMMMMMMMMMMMMMMMMMMMM           Packages: 471 (rpm)
  .MMMM'  MMMMMMMMMMMMMMMMMMMMMM           Shell: bash 4.2.46
 MMMMMM    `MMMMMMMMMMMMMMMMMMMM.          Terminal: /dev/pts/0
MMMMMMMM      MMMMMMMMMMMMMMMMMM .         CPU: 8561 (2)
MMMMMMMMM.       `MMMMMMMMMMMMM' MM.       Memory: 1150MiB / 3847MiB
MMMMMMMMMMM.                     MMMM
`MMMMMMMMMMMMM.                 ,MMMMM.
 `MMMMMMMMMMMMMMMMM.          ,MMMMMMMM.
    MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
      MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM:
         MMMMMMMMMMMMMMMMMMMMMMMMMMMMMM
            `MMMMMMMMMMMMMMMMMMMMMMMM:
                ``MMMMMMMMMMMMMMMMM'
```
### 宝塔
可以用，但是不能用官方的方法，安装方法如下

RHEL
```
# 复制所有命令粘贴到终端
sudo su
iptables -P INPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -P OUTPUT ACCEPT
iptables -F
iptables-save
yum install -y wget curl && wget -O install.sh http://download.bt.cn/install/install_6.0.sh && sh install.sh ed8484bec
btpip install gevent
btpip install flask
btpip install gevent-websocket
btpip install psutil
cd /tmp
curl https://raw.githubusercontent.com/MBRjun/btpanel-v7.7.0/main/install/install_panel.sh --output install_panel.sh
bash install_panel.sh
sed -i "s|if (bind_user == 'True') {|if (bind_user == 'REMOVED') {|g" /www/server/panel/BTPanel/static/js/index.js
btpip install requests
rm install_panel.sh
```
Ubuntu
```
# 复制所有命令粘贴到终端
sudo su
iptables -P INPUT ACCEPT
iptables -P FORWARD ACCEPT
iptables -P OUTPUT ACCEPT
iptables -F
iptables-save
wget -O install.sh http://download.bt.cn/install/install-ubuntu_6.0.sh && sudo bash install.sh ed8484bec
btpip install gevent
btpip install flask
btpip install gevent-websocket
btpip install psutil
wget https://raw.githubusercontent.com/MBRjun/btpanel-v7.7.0/main/install/install_panel.sh
bash install_panel.sh
sed -i "s|if (bind_user == 'True') {|if (bind_user == 'REMOVED') {|g" /www/server/panel/BTPanel/static/js/index.js
btpip install requests
rm install_panel.sh
```
### SpeedTest
```
#二进制版本不支持 S390x，请使用包管理器
curl -s https://install.speedtest.net/app/cli/install.rpm.sh | sudo bash
sudo yum install speedtest
speedtest

    Latency:    17.57 ms   (0.14 ms jitter)
   Download:   1.15 Gbps (data used: 1.75 GB )
     Upload:     0.50 Gbps (data used: 0.71 GB )
Packet Loss:     0.0%

```
