---
title: Azure云创建API
date: 2022-08-26 17:51:38
tags:
    - azure
categories: 技术教程
---
在Azure云上创建API可以帮助用户快速构建自己的API服务，提供各种数据和功能的访问接口。Azure云是微软提供的云计算平台，提供了强大的计算、存储和网络资源，支持多种编程语言和框架。用户只需在Azure云上选择相应的服务和配置，然后编写API代码，即可快速部署和发布自己的API服务。通过这种方式，用户可以更方便地实现各种数据和功能的访问，提高应用程序的灵活性和可扩展性。
<!--more-->
# 安装Azure Cli
官网教程：
https://docs.microsoft.com/en-us/cli/azure/install-azure-cli

# 登录Azure
```
az login
```

# 创建API访问权限
```
az ad sp create-for-rbac --name api
```
保存参数
